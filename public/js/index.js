function init() {
    var user_email = '';
    var username = document.getElementById("usernamedisplay");
    var btnLoginout = document.getElementById("loginout");
    var content = document.getElementById("content");
    
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            username.innerHTML = "<span class='navbar-text'><img src='img/user.png' width='20' height='20'> " + user_email + "</span>";
            btnLoginout.innerHTML = "<a class='nav-link' href='index.html'><img src='img/logout.png' width='20' height='20'> Logout</a>";
            content.innerHTML = "<br><h1>Welcome to Chatroom</h1><br><p>Hi, " + user_email + ". Click <a href='chat.html'><span class='hrefstyle'>here</span></a> to join the chat!</p><br>";
            btnLoginout.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Successfully sign out.')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            username.innerHTML = "";
            btnLoginout.innerHTML = "<a class='nav-link' href='login.html'><img src='img/login.png' width='20' height='20'> Login</a>";
        }
    });

}

window.onload = function () {
    init();
}