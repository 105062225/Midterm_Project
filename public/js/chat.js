function initApp() {
    //user state
    
    //var photoUrl = firebase.storage().ref('defaultPic/' + anon.png).getDownloadURL();
    var photoUrl = '';
    var user_email = '';
    var profilePic = document.getElementById('profPic');
    var username = document.getElementById("usernamedisplay");
    var btnLoginout = document.getElementById("loginout");

    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            
            if(user.photoURL==null) {
                profilePic.src = "img/anon.png";
                photoUrl = "img/anon.png";
            }
            else {
                profilePic.src = user.photoURL;
                photoUrl = user.photoURL;
            }
            user_email = user.email;
            username.innerHTML = "<span class='navbar-text'><img src='img/user.png' width='20' height='20'> " + user_email + "</span>";
            btnLoginout.innerHTML = "<a class='nav-link' href='index.html'><img src='img/logout.png' width='20' height='20'> Logout</a>";
            btnLoginout.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Successfully sign out.')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            username.innerHTML = "";
            btnLoginout.innerHTML = "<a class='nav-link' href='login.html'><img src='img/login.png' width='20' height='20'> Login</a>";
        }
    });
    //chat
    
    send_btn = document.getElementById('btnSend');
    post_txt = document.getElementById('comment');
    
    


    send_btn.addEventListener('click', function () {
        var dt = new Date();
        var year = 1900 + dt.getYear();
        var month = 1 +dt.getMonth();
        var date = dt.getDate();
        var hour = dt.getHours();
        var min = dt.getMinutes();
        var sec = dt.getSeconds();
        if(hour<10) hour = "0"+hour;
        if(min<10) min = "0"+min;
        if(sec<10) sec = "0"+sec;
        var postAt = year + "/" + month + "/" + date + " " + hour + ":" + min + ":" + sec;

        if (post_txt.value != "") {
            var newpostref = firebase.database().ref('com_list').push();
            newpostref.set({
                email: user_email,
                data: post_txt.value,
                timestamp: postAt,
                pic: photoUrl
            });
            post_txt.value = "";
        }
    });
    var str_before_username = "<div class='media text-muted pt-3 px-3'><img src='";
    var str_2 = "' class='mr-2 rounded' style='height:32px; width:32px;'><p class='message rounded media-body pb-3 px-1 mb-0 small  border-bottom border-gray'><strong class='d-inline text-gray-dark'>";

    var str_after_content = "</p></div>\n";
    

    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    postsRef.once('value')
        .then(function (snapshot) {
            snapshot.forEach(function (childSnapshot) {
                var childData = childSnapshot.val();
                total_post[total_post.length] = str_before_username+ childData.pic + str_2+ childData.email + "</strong> " + childData.timestamp + "<br>" +childData.data + str_after_content;
                first_count += 1;
            });
            document.getElementById('chatHistory').innerHTML = total_post.join('');

            //add listener
            postsRef.on('child_added', function (data) {
                second_count += 1;
                if (second_count > first_count) {
                    var childData = data.val();
                    total_post[total_post.length] = str_before_username+ childData.pic + str_2 + childData.email + "</strong> " + childData.timestamp + "<br>" + childData.data + str_after_content;
                    document.getElementById('chatHistory').innerHTML = total_post.join('');
                }
            });
        })
        .catch(e => console.log(e.message));

    //upload pic
    var fileButton = document.getElementById('fileButton');

    fileButton.addEventListener('change', function(e){
        var file = e.target.files[0];
        var storageRef = firebase.storage().ref('Images/' + file.name);
        var task = storageRef.put(file);
        
          firebase.storage().ref('Images/' + file.name).getDownloadURL().then(function(url) {
            profilePic.src = url;
            alert("upload success");
            var user = firebase.auth().currentUser;
            user.updateProfile({
                photoURL: url
                }).then(function() {
                    alert("update success");
                    photoUrl = user.photoURL;
                }).catch(function(error) {
                    alert(error.message);
            });
          }).catch(function(error) {
            // Handle any errors
                alert(error.message);
          }); 

          
        
          
    });



    // Get a reference to the storage service, which is used to create references in your storage bucket
         //var storage = firebase.storage();
    // Create a storage reference from our storage service
         //var storageRef = storage.ref();
    // Create a child reference
         //var profilePicRef = storageRef.child('profilePic.jpg');
    // imagesRef now points to 'images'
    // Child references can also take paths delimited by '/'
        //var profilePicImgRef = storageRef.child('images/profilePic.jpg');
    // spaceRef now points to "images/space.jpg

    // Create file metadata including the content type
}


window.onload = function () {
    initApp();
}

function pageScroll() {
    document.getElementById("publicChat").scrollBy(0,5);
    scrolldelay = setTimeout('pageScroll()', 1); 
    document.getElementById("chatHistory").onclick = function () {stopScroll() };
}

function stopScroll() {
    clearTimeout(scrolldelay);
    document.getElementById("chatHistory").onclick = function () {pageScroll()};
}
