function initApp() {
    //user state
    var user_email = '';
    var username = document.getElementById("usernamedisplay");
    var btnLoginout = document.getElementById("loginout");
    
    firebase.auth().onAuthStateChanged(function (user) {
        if (user) {
            user_email = user.email;
            username.innerHTML = "<span class='navbar-text'><img src='img/user.png' width='20' height='20'> " + user_email + "</span>";
            btnLoginout.innerHTML = "<a class='nav-link' href='index.html'><img src='img/logout.png' width='20' height='20'> Logout</a>";
            btnLoginout.addEventListener('click', function () {
                firebase.auth().signOut()
                    .then(function () {
                        alert('Successfully sign out.')
                    })
                    .catch(function (error) {
                        alert('Sign Out Error!')
                    });
            });
        } else {
            username.innerHTML = "";
            btnLoginout.innerHTML = "<a class='nav-link' href='login.html'><img src='img/login.png' width='20' height='20'> Login</a>";
        }
    });
    //sign up
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnSignUp = document.getElementById('btnSignUp');


    btnSignUp.addEventListener('click', function () {
        var email = txtEmail.value;
        var password = txtPassword.value;
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(function () {
                create_alert("success", "You could sign in  right now!");
                txtEmail.value = "";
                txtPassword.value = "";
            })
            .catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                create_alert("error", errorMessage);
                txtEmail.value = "";
                txtPassword.value = "";
            });
    });

}

function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
    else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
}