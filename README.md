# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* a typical Chat room
* Key functions (add/delete)
    1. load message history
    2. chat
    3. timestamp
    4. chat with new user
* Other functions (add/delete)
    1. Sign Up/In with Google or other third-party accounts
    2. CSS animation
    3. Can change user profile picture
    4. Click to scroll down

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description

## Security Report (Optional)
